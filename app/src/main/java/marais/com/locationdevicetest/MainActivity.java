package marais.com.locationdevicetest;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class MainActivity extends AppCompatActivity {
    private TextView status;
    private GoogleApiClient mGoogleApiClient;
    private LocationManager locationManagerLegacy;
    private LocationListener legacyLocationListener;
    private Context context;
    private static int timeout = 10000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context = this;

        status = (TextView) findViewById(R.id.status);
        addStatus("onCreate()");
        addStatus("-----------------");

        addStatus("timeouts set for: " + timeout + "ms");

        isGooglePlayServicesAvailable();
        legacyLocationManagerGps();
        // will start fused location after timeout for legacy
    }

    private void addStatus(String s) {
        if (status != null) {
            status.setText(status.getText() + s + "\n\n");
        }
    }

    private void fusedLocationGps() {
        // timeout for legacyLocationManagerGps()
        final Handler fusedLocationHandler = new Handler();
        final Runnable fusedLocationRunnable = new Runnable() {
            @Override
            public void run() {
                if (mGoogleApiClient != null &&
                        mGoogleApiClient.isConnected()) {
                    addStatus("fusedLocationGps() timeout");
                    addStatus("-----------------");
                    mGoogleApiClient.disconnect();
                }

                // next location type
            }
        };
        fusedLocationHandler.postDelayed(fusedLocationRunnable, timeout);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(new GoogleApiClient.ConnectionCallbacks() {
                    @Override
                    public void onConnected(@Nullable Bundle bundle) {
                        addStatus("fusedLocationGps() onConnected()");

                        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                                ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            ActivityCompat.requestPermissions(
                                    MainActivity.this,
                                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                    1);

                            return;
                        }

                        // last location
                        Location location = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                        if (location != null) {
                            addStatus("SUCCESS fusedLocationGps() getLastLocation() "
                                    + "lat: "
                                    + location.getLatitude()
                                    + ", lng: "
                                    + location.getLongitude());

                            // stop timeout on success
                            fusedLocationHandler.removeCallbacks(fusedLocationRunnable);
                            addStatus("-----------------");
                        } else {
                            addStatus("fusedLocationGps() getLastLocation() null");
                        }

                        // ---------------------------------------

                        // get new fused location
                        if (location == null) {
                            addStatus("fusedLocationGps() new location request");
                            LocationRequest mLocationRequest = LocationRequest.create()
                                    .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                                    .setInterval(10 * 1000)        // 10 seconds, in milliseconds
                                    .setFastestInterval(1 * 1000); // 1 second, in milliseconds

                            LocationServices.FusedLocationApi.requestLocationUpdates(
                                    mGoogleApiClient,
                                    mLocationRequest,
                                    new com.google.android.gms.location.LocationListener() {
                                        @Override
                                        public void onLocationChanged(Location location) {
                                            if (location != null) {
                                                addStatus("SUCCESS fusedLocationGps() onLocationChanged() lat:"
                                                        + location.getLatitude()
                                                        + ", lng: "
                                                        + location.getLongitude());

                                                // stop timeout on success
                                                fusedLocationHandler.removeCallbacks(fusedLocationRunnable);
                                                addStatus("-----------------");

                                                if (mGoogleApiClient != null &&
                                                        mGoogleApiClient.isConnected()) {
                                                    LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                                                    mGoogleApiClient.disconnect();
                                                }
                                            }
                                        }
                                    });
                        }
                    }

                    @Override
                    public void onConnectionSuspended(int i) {
                        addStatus("fusedLocationGps() onConnectionSuspended()");
                    }
                })
                .addOnConnectionFailedListener(new GoogleApiClient.OnConnectionFailedListener() {
                    @Override
                    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                        addStatus("fusedLocationGps() onConnectionFailed()");
                    }
                })
                .build();

        mGoogleApiClient.connect();
        addStatus("fusedLocationGps() mGoogleApiClient.connect()");
    }

    private void legacyLocationManagerGps() {
        // timeout for legacyLocationManagerGps()
        final Handler legacyHandler = new Handler();
        final Runnable legacyRunnable = new Runnable() {
            @Override
            public void run() {
                if (locationManagerLegacy != null) {
                    addStatus("legacyLocationManagerGps() timeout");
                    addStatus("-----------------");

                    if (legacyLocationListener != null) {
                        locationManagerLegacy.removeUpdates(legacyLocationListener);
                    }
                }

                // next location type
                fusedLocationGps();
            }
        };
        legacyHandler.postDelayed(legacyRunnable, timeout);

        addStatus("legacyLocationManagerGps() start");
        locationManagerLegacy = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED &&
                ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            ActivityCompat.requestPermissions(
                    MainActivity.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    1);

            return;
        }

        legacyLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    addStatus("SUCCESS legacyLocationManagerGps() onLocationChanged() lat:"
                            + location.getLatitude()
                            + ". lng: "
                            + location.getLongitude());

                    if (legacyLocationListener != null) {
                        locationManagerLegacy.removeUpdates(legacyLocationListener);
                    }
                    // stop timeout on success
                    legacyHandler.removeCallbacks(legacyRunnable);

                    addStatus("legacyLocationManagerGps() stop");
                    addStatus("-----------------");

                    // try fused location even if legacy was successfull
                    fusedLocationGps();
                }
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {
                addStatus("legacyLocationManagerGps() onStatusChanged()");
            }

            @Override
            public void onProviderEnabled(String s) {
                addStatus("legacyLocationManagerGps() onProviderEnabled()");
            }

            @Override
            public void onProviderDisabled(String s) {
                addStatus("legacyLocationManagerGps() onProviderDisabled()");
            }
        };
        locationManagerLegacy.requestLocationUpdates(
                LocationManager.GPS_PROVIDER,
                1000,
                0,
                legacyLocationListener);
    }

    private void isGooglePlayServicesAvailable() {
        int status = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(this);
        if (ConnectionResult.SUCCESS == status) {
            addStatus("isGooglePlayServicesAvailable() true");
        } else {
            addStatus("isGooglePlayServicesAvailable() false");
        }
        addStatus("-----------------");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        // reload activity
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }
}
